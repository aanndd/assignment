//
//  User.swift
//  MVVM_Assignment
//
//  Created by Anand Yadav on 21/02/22.
//

import UIKit

struct User: Codable {
    let userID, id: Int
    let title, body: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}
