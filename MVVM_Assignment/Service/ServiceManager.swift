//
//  ServiceManager.swift
//  MVVM_Assignment
//
//  Created by Anand Yadav on 21/02/22.
//

import UIKit

class ServiceManager: NSObject {
    
    static func getUsers() -> [User]{

        let user = [User].map(JSONData:Helper().readLocalFile(forName: "User")! as Data)!
        return user

    }
}
