//
//  AppExtensions.swift
//  MVVM_Assignment
//
//  Created by Anand Yadav on 21/02/22.
//

import UIKit

extension Decodable {
    static func map(JSONData:Data) -> Self? {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(Self.self, from: JSONData)
        } catch let error {
            print(error)
            return nil
        }
    }
}
