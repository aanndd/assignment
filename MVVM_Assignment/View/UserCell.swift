//
//  UserCell.swift
//  MVVM_Assignment
//
//  Created by Anand Yadav on 21/02/22.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var lblUserID: UILabel!
    @IBOutlet weak var lblUserTitle: UILabel!
    
    var user: User?{
        didSet{
            configureUserCell()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureUserCell(){
        lblUserID.text = "\(user!.id)"
        lblUserTitle.text = user?.title
    }

}
