//
//  DetailsViewController.swift
//  MVVM_Assignment
//
//  Created by Anand Yadav on 21/02/22.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var lblUserDetails: UILabel!
    var userDetails:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        lblUserDetails.text = userDetails
        print(userDetails)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
