//
//  UserViewModel.swift
//  MVVM_Assignment
//
//  Created by Anand Yadav on 21/02/22.
//

import UIKit

class UserViewModel {
    var user = [User]()
    
    func getAllUsers() {
        user = ServiceManager.getUsers()
    }
    
    func getUserDetails(userID:Int) -> String {
        var userDetails:String?
        for user in ServiceManager.getUsers() {
            if user.userID == userID {
                userDetails = user.body
                return userDetails!
            }
        }
        return "No details found!"
    }
}
